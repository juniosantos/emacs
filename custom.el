;;; custom.el --- Custom variables and Emacs "don't ask me again" things

;;; Commentary: To turn this file more useful I wil also append
;;; some custom variables to make use in my entire file.

(defvar path-backup (+append-with-config "backup/"))
(defvar path-modules (+append-with-config "modules/"))
(defvar path-lisp (+append-with-config "lisp/"))

;;; custom.el ends here
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(corfu-bar-width 0.4)
 '(corfu-max-width 80)
 '(corfu-min-width 80)
 '(corfu-on-exact-match 'quit)
 '(visual-fill-column-width 70))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(fixed-pitch ((t :family "Iosevka" :height 160)))
 '(markdown-header-face ((t (:inherit font-lock-function-name-face :weight bold :family Iosevka))))
 '(markdown-header-face-1 ((t (:inherit markdown-header-face :height 1.5))))
 '(markdown-header-face-2 ((t (:inherit markdown-header-face :height 1.4))))
 '(markdown-header-face-3 ((t (:inherit markdown-header-face :height 1.3))))
 '(markdown-header-face-4 ((t (:inherit markdown-header-face :height 1.2))))
 '(markdown-header-face-5 ((t (:inherit markdown-header-face :height 1.0))))
 '(markdown-header-face-6 ((t (:inherit markdown-header-face :height 1.0))))
 '(org-block ((t (:inherit fixed-pitch))))
 '(org-block-begin-line ((t (:inherit fixed-pitch))))
 '(org-block-end-line ((t (:inherit fixed-pitch))))
 '(org-code ((t (:inherit (shadow fixed-pitch)))))
 '(org-date ((t (:inherit (shadow fixed-pitch)))))
 '(org-document-info ((t (:inherit (shadow fixed-pitch)))))
 '(org-document-info-keyword ((t (:inherit (shadow fixed-pitch)))))
 '(org-document-title ((t (:inherit default :weight bold :underline t :family "PT Serif"))))
 '(org-drawer ((t (:inherit (shadow fixed-pitch)))))
 '(org-ellipsis ((t (:inherit fixed-pitch :height 0.8 :weight normal :foreground "gray40" :underline nil))))
 '(org-indent ((t (:inherit (org-hide fixed-pitch)))))
 '(org-level-1 ((t (:inherit default :weight bold :underline t :family "PT Serif"))))
 '(org-level-2 ((t (:inherit default :weight bold :underline t :family "PT Serif"))))
 '(org-level-3 ((t (:inherit default :weight bold :underline t :family "PT Serif"))))
 '(org-level-4 ((t (:inherit default :weight bold :underline t :family "PT Serif"))))
 '(org-level-5 ((t (:inherit default :weight bold :underline t :family "PT Serif"))))
 '(org-level-6 ((t (:inherit default :weight bold :underline t :family "PT Serif"))))
 '(org-level-7 ((t (:inherit default :weight bold :underline t :family "PT Serif"))))
 '(org-level-8 ((t (:inherit default :weight bold :underline t :family "PT Serif"))))
 '(org-link ((t (:inherit fixed-pitch :foreground "blue" :underline t))))
 '(org-meta-line ((t (:inherit (font-lock-comment-face fixed-pitch)))))
 '(org-properties ((t (:inherit fixed-pitch))))
 '(org-property-value ((t (:inherit fixed-pitch))) t)
 '(org-special-keyword ((t (:inherit (font-lock-comment-face fixed-pitch)))))
 '(org-src ((t (:inherit fixed-pitch))))
 '(org-table ((t (:inherit fixed-pitch))))
 '(org-tag ((t (:inherit (shadow fixed-pitch) :weight bold :height 0.8))))
 '(org-verbatim ((t (:inherit (shadow fixed-pitch)))))
 '(variable-pitch ((t :family "PT Serif"))))
