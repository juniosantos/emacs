  (use-package nix-mode
    :defer t
    :mode "\\.nix\\'"
    :config
    (electric-indent-mode -1))
