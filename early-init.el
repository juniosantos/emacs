;;; early-init.el --- Personal configuration file -*- lexical-binding: t -*-

;; Copyright (c) 2021-2023  Junio Santos <info@junio.dev>

;; Author: Junio Santos <info@junio.dev>
;; URL: https://junio.dev/dotemacs
;; Version: 0.2.0
;; Package-Requires: ((emacs "30.1"))

;; This file is NOT part of GNU Emacs.

;; This file is free software: you can redistribute it and/or modify it
;; under the terms of the GNU General Public License as published by the
;; Free Software Foundation, either version 3 of the License, or (at
;; your option) any later version.
;;
;; This file is distributed in the hope that it will be useful, but
;; WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; General Public License for more details.
;;
;; You should have received a copy of the GNU General Public Licenseg
;; along with this file.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; This file is loaded even when Emacs is started in debug mode
;; making essential settings usable when debugging and also
;; can be modified to make startup faster (or slower)
;; and all settings defined here can be used for the rest of application
;; Have fun ;)

;; Enable a daemon for faster launch
;;;; Load lisp utilities
(load-file (expand-file-name "lisp/common.el" user-emacs-directory))
;;

;;;; Store automatically custom variables without changing init.el
(let ((_custom (+append-with-config "custom.el")))
  (unless (file-exists-p _custom)
    (make-empty-file _custom))
  (when (file-exists-p _custom)
    (setq custom-file _custom)
    (load _custom)))
;;

;;;; Create backup folder to a place to throw litter
(let ((path (+append-with-config "backup/lockfiles/")))
  (unless (file-directory-p path)
    (make-directory path))
  (setq lock-file-name-transforms '((".*" "~/.config/emacs/backup/lockfiles/" t))))
;;

;; Get rid of `#exemple.el#' files
(setq history-delete-duplicates t)
(setq history-length t   ; Default is 30
      kill-ring-max 5000   ; Truncate kill ring after 5000 entries
      mark-ring-max 5000)  ; Truncate mark ring after 5000 entries
;;
;;;; Don't create litter while editing
(progn
  (let ((_custom (+append-with-config "backup/emacs-auto-save-file/")))
    (unless (file-exists-p _custom)
      (make-empty-file _custom)))
  (setq savehist-save-minibuffer-history 1)
  (setq savehist-additional-variables '(kill-ring
					search-ring
					register-alist
					regexp-search-ring))
  (setq savehist-file (+append-with-config "backup/savehist"))
  (setq auto-save-file-name-transforms '((".*" "~/.config/emacs/backup/emacs-auto-save-file/" t)))
  (savehist-mode 1))

(setq backup-directory-alist '(("." . "~/.config/emacs/backup/backup-file-")))

(setq frame-resize-pixelwise t
      frame-inhibit-implied-resize t
      use-dialog-box nil ; only for mouse events, which I seldom use
      use-file-dialog nil
      inhibit-splash-screen t
      inhibit-startup-screen t
      inhibit-x-resources t
      inhibit-startup-echo-area-message user-login-name ; read the docstring
      inhibit-startup-buffer-menu t)

;; I don't like much the visual of default emacs UI tools so
;; I disable it asap
(menu-bar-mode -1)
(scroll-bar-mode -1)
(tool-bar-mode -1)

;; Temporarily increase the garbage collection threshold.  These
;; changes help shave off about half a second of startup time.
(defvar junio-emacs--gc-cons-threshold gc-cons-threshold)

(setq gc-cons-threshold most-positive-fixnum)

;; Same idea as above for the `file-name-handler-alist'.
(defvar junio-emacs--file-name-handler-alist file-name-handler-alist)

(setq file-name-handler-alist nil)

(add-hook 'emacs-startup-hook
          (lambda ()
            (setq gc-cons-threshold junio-emacs--gc-cons-threshold
                  file-name-handler-alist junio-emacs--file-name-handler-alist)))

;; Do not initialise installed packages at this early stage.
(setq package-enable-at-startup nil)

(defvar junio-emacs-theme-environment-dark-p t)

(defun junio-emacs-re-enable-frame-theme (_frame)
  "Re-enable active theme, if any, upon FRAME creation.
Add this to `after-make-frame-functions' so that new frames do
not retain the generic background set by the function
`prot-emacs-avoid-initial-flash-of-light'."
  (when-let ((theme (car custom-enabled-themes)))
    (enable-theme theme)))

;; The reason the following works is because (i) the
;; `mode-line-format' is specified again and (ii) the
;; `prot-emacs-theme-gsettings-dark-p' will load a dark theme.
(defun junio-emacs-avoid-initial-flash-of-light ()
  "Avoid flash of light when starting Emacs, if needed.
New frames are instructed to call `prot-emacs-re-enable-frame-theme'."
  (when #'junio-emacs-theme-environment-dark-p
    (setq mode-line-format nil)
    (set-face-attribute 'default nil :background "#000000" :foreground "#ffffff")
    (set-face-attribute 'mode-line nil :background "#000000" :foreground "#ffffff" :box 'unspecified)
    (add-hook 'after-make-frame-functions #'junio-emacs-re-enable-frame-theme)))

(junio-emacs-avoid-initial-flash-of-light)

(add-hook 'after-init-hook (lambda () (set-frame-name "home")))

;;; early-init.el ends here
