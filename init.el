;;; init.el --- Personal configuration file -*- lexical-binding: t -*-

;; Copyright (c) 2021-2023  Junio Santos <info@junio.dev>

;; Author: Junio Santos <info@junio.dev>
;; URL: https://junio.dev/dotemacs
;; Version: 0.2.0
;; Package-Requires: ((emacs "30.1"))

;; This file is NOT part of GNU Emacs.

;; This file is free software: you can redistribute it and/or modify it
;; under the terms of the GNU General Public License as published by the
;; Free Software Foundation, either version 3 of the License, or (at
;; your option) any later version.
;;
;; This file is distributed in the hope that it will be useful, but
;; WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; General Public License for more details.
;;
;; You should have received a copy of the GNU General Public Licenseg
;; along with this file.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; Currently I've been using some configuration as reference
;; from Sacha Chu(i) and Protesilaos Stravou(ii). Thank you very much 
;; (i): <https://git.sr.ht/~protesilaos/dotfiles>
;; (ii): <TODO>

;; See my dotfiles: <https://junio.dev/dotfiles>

;;;; Code:
(setq debug-on-error t)
(defvar my-server-p (and (equal (system-name) "localhost") (equal user-login-name "junio")))
(defvar modules-path  (expand-file-name "modules/" user-emacs-directory))

(require 'server)
(unless (server-running-p)
  (server-start))

(setq user-full-name "Junio Santos"
      user-mail-address "info@junio.dev")

;; `'Straight.el' :: Package Manager
(defvar bootstrap-version)
(let ((bootstrap-file
       (expand-file-name "straight/repos/straight.el/bootstrap.el" user-emacs-directory))
      (bootstrap-version 6))
  (unless (file-exists-p bootstrap-file)
    (with-current-buffer
	(url-retrieve-synchronously
	 "https://raw.githubusercontent.com/radian-software/straight.el/develop/install.el"
	 'silent 'inhibit-cookies)
      (goto-char (point-max))
      (eval-print-last-sexp)))
  (load bootstrap-file nil 'nomessage))
(setq load-prefer-newer t)

;; Enable Straight to make use of `package.el'
(straight-use-package 'use-package)
(setq straight-use-package-by-default t)
(require 'package)
(add-to-list 'package-archives
             '("melpa" . "https://melpa.org/packages/"))

;; Guarantee straight use builtin certain tools
(use-package org :straight (:type built-in))

;; Essential packages
(use-package diminish)

;;;; Set default mode of the scratch buffer to Org
(setq initial-major-mode 'org-mode)
(setq initial-scratch-message "\
#+TITLE: Scratch
#+STARTUP: inlineimages
# This buffer is for notes you don't want to save. You can use
# org-mode markup (and all Org's goodness) to organise the notes.
# If you want to create a file, visit that file with C-x C-f,
# then enter the text in that file's own buffer.


[[~/Pictures/dolphin.png]]

*Elisp*
#+begin_src elisp

#+end_src

*Notes:*
-")

;;;; Basic settings
(setq frame-title-format '("%b"))
(setq ring-bell-function 'ignore)
(setq use-short-answers t)
(setq native-comp-async-report-warnings-errors 'silent)
(setq native-compile-prune-cache t) ; Emacs 29

;;;; Backup
(with-eval-after-load 'tramp
  (add-to-list 'tramp-backup-directory-alist
	       (cons tramp-file-name-regexp nil)))

;; Enable these
(dolist (c '(narrow-to-region narrow-to-page upcase-region downcase-region))
  (put c 'disabled nil))

;; And disable these
(dolist (c '(eshell project-eshell overwrite-mode iconify-frame diary))
  (put c 'disabled t))

(defalias 'yes-or-no-p 'y-or-n-p)

;; Browser
(use-package eww)
(use-package keychain-environment)
(use-package restart-emacs
  :config
  (defun my-reload-emacs-config ()
    (interactive)
    (load-file user-init-file))
  
  (defun my-restart-emacs()
    (interactive)
    (my-reload-emacs-config)
    (restart-emacs)))

(use-package recentf
  :straight (:type built-in)
  :custom
  (recentf-max-saved-items 200)
  (recentf-max-menu-items 25)
  :config
  (recentf-mode))


(setq ;; Smooth scrolling
      mouse-wheel-scroll-amount '(1 ((shift) . 1) ((control)))
      mouse-wheel-progressive-speed nil
      scroll-conservatively 10
      scroll-preserve-screen-position 1
      fast-but-imprecise-scrolling t)

(progn
  (jun/load-module 'essential)
  (jun/load-module 'eglot)
  (jun/load-module 'completion)

  (jun/load-module 'dired)
  (jun/load-module 'ibuffer)
  (jun/load-module 'org)
  (jun/load-module 'vc)
  (jun/load-module 'eshell)

  (jun/load-module 'ui)
  (jun/load-module 'keymaps)  ;; Keymaps should always be the last
  )

;;; end of init.el
