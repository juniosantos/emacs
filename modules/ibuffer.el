;;; ibuffer --- intelligent buffer layout

(use-package ibuffer
  :straight (:type built-in)
  :config
  (add-hook 'ibuffer-mode-hook
	    (lambda ()
	      (ibuffer-switch-to-saved-filter-groups "default")
	      (ibuffer-update nil t)))
  :custom
  (ibuffer-use-header-line t)
  (ibuffer-expert t)
  (ibuffer-show-empty-filter-groups nil)
  (ibuffer-saved-filter-groups
   '(("default"
      ;; ("Modified" (predicate buffer-modified-p (current-buffer)))
      ("Dired" (mode . dired-mode))
      ("Notes" (or (filename . "^~/notes/")
		   (filename . "^~/docs/")))
      ("Config" (filename . "^.+/\\.config/emacs/.+$"))
      ("Magit" (name . "magit\\*"))
      ("Rust"
       (predicate . (and (eq major-mode 'rust-mode)
                         (not (string-match-p "spec\\.rs\\'"
                                              buffer-file-name)))))
      ("Tests" (name . "spec\\.rs\\'"))

      ("Emacs" (or (name . "^\\*scratch\\*$")
		   (name . "^\\*Backtrace\\*$")
		   (name . "^\\*Compile-log\\*$")
                   (name . "^\\*Messages\\*$")))
      
      ("Help" (or (mode . helpful-mode)
		  (mode . help-mode)))


      ("Gnus" (or
               (mode . message-mode)
               (mode . bbdb-mode)
               (mode . mail-mode)                                                       
               (mode . gnus-group-mode)
               (mode . gnus-summary-mode)
               (mode . gnus-article-mode)
               (name . "^\\.bbdb$")
               (name . "^\\.newsrc-dribble")))
      ("*Internal*" (name . "\\*"))
     )))

  ;; Modify the default ibuffer-formats
  (ibuffer-formats
   '((mark modified read-only locked " "
	   (name 20 20 :left :elide)
	   " "
	   (filename-and-process 40 -1)
	   " "
	   (+jun/size-h 12 -1 :right :elide)
	   "| "
	   (mode 20 -1))
     (mark " "
	   (name 20 6)
	   " " filename))));;

(setq mp/ibuffer-collapsed-groups (list "Emacs" "*Internal*"))

(defadvice ibuffer (after collapse-helm)
  (dolist (group mp/ibuffer-collapsed-groups)
	  (progn
	    (goto-char 1)
	    (when (search-forward (concat "[ " group " ]") (point-max) t)
	      (progn
		(move-beginning-of-line nil)
		(ibuffer-toggle-filter-group)
		)
	      )
	    )
	  )
    (goto-char 1)
    (search-forward "[ " (point-max) t)
  )

(ad-activate 'ibuffer)
;;; ibuffer.el end of file
