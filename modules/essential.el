;;; essential.el --- Group of miscelanious commands that makes the whole difference
;; smooooth
(setq-default smooth-scroll-margin 0)
(setq   vc-follow-symlinks t)
(setq   mouse-yank-at-point t)

;; Electric 🔌
(defun toggle-electric-mode ()
  (interactive)
  (electric-pair-mode)
  (electric-quote-mode)
  (electric-layout-mode))

(unless (electric-pair-mode)
  (toggle-electric-mode))

;;(use-package )
(use-package rainbow-mode)
(use-package devdocs)
(use-package easy-kill
  :diminish easy-mark
  :config
  (global-set-key [remap kill-ring-save] 'easy-mark))

(global-prettify-symbols-mode +1)

(delete-selection-mode t)
(setq cursor-type 'box)  ;; (box, bar, hbar)

(setq delete-pair-blink-delay 0.15) ; Emacs28 -- see `prot-simple-delete-pair-dwim'
(setq help-window-select nil) ; don't go to help window automatically
(setq next-error-recenter '(4)) ; center of the window
(setq find-library-include-other-files nil) ; Emacs 29
(setq remote-file-name-inhibit-delete-by-moving-to-trash t) ; Emacs 30
(setq remote-file-name-inhibit-auto-save t)                 ; Emacs 30


;; ㊙️
(prefer-coding-system 'utf-8)
(when (display-graphic-p)
  (setq x-select-request-type '(UTF8_STRING COMPOUND_TEXT TEXT STRING)))

(lambda () ((undelete-frame-mode)
	    (whitespace-newline-mode)))

(setq whitespace-display-mappings
      '((newline-mark ?\n [?¬ ?\n]))) ;; ↵ ¶ ¯ ¬ °
;(set-face-attribute 'whitespace-newline :foreground 'f0f0f0)

;;;; Mouse Wheel behavior
(setq mouse-wheel-scroll-amount
      '(1
        ((shift) . 5)
        ((meta) . 0.5)
        ((control) . text-scale))
      mouse-drag-copy-region nil
      make-pointer-invisible t
      mouse-wheel-progressive-speed t
      mouse-wheel-follow-mouse t)

(defun toggle-display-time ()
  (interactive)
  (progn
    (display-time-mode)
    (setq display-time-load-average nil)
    (setq display-time-interval 1)
    (setq display-time-24hr-format t)
    (setq display-time-format ":: %H:%M:%S :: %d/%m/%y |")
    (setq display-time-day-and-date t)))

(unless display-time-mode
  (toggle-display-time))

;;;; Bookmarks
(setq bookmark-automatically-show-annotations t)
(setq bookmark-fringe-mark nil) ; Emacs 29 to hide bookmark fringe icon
(add-hook 'bookmark-bmenu-mode-hook #'hl-line-mode)

;; Save all buffers when Emacs lose focus
;;(require 'sly)				;
;; (add-hook 'focus-out-hook
;; (lambda ()
;; (cl-flet* ((message (format &rest args) nil)) ;; avoid trashing minibuffer
;; (save-some-buffers t))))

(use-package xclip :config (xclip-mode))
(use-package helpful)
(use-package elcord
  :diminish elcord-mode ;; Reduce messages if discord is closed
  :config
  (setq elcord-quiet t)
  (elcord-mode))

(defun bookmark-save-no-prompt (&rest _)
  (funcall 'bookmark-save))
(advice-add 'bookmark-set-internal :after 'bookmark-save-no-prompt)

(when (display-graphic-p)
  (desktop-save-mode 1);; is x window
  ())
(add-to-list 'desktop-globals-to-save 'register-alist)
