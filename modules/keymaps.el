;;; keymaps.el --- Where all keymaps are defined
;;
;;-
;; Plugins
;;

(use-package general
  :after evil
  :config
  (general-create-definer my-leader-keys
    :keymaps '(normal insert visual emacs)
    :prefix "SPC"
    :global-prefix "C-c"))

(use-package which-key
  :defer 0
  :diminish which-key-mode
  :custom
  (which-key-show-early-on-C-h t)
  (which-key-idle-delay 1)
  (which-key-idle-secondary-delay 0.01)
  (which-key-special-keys '("SPC" "TAB" "RET" "ESC" "DEL" ";"))
  (which-key-unicode-correction 3)
  (which-key-show-remaining-keys t)
  (which-key-max-display-columns 5)

  (which-key-add-column-padding 10)
  (which-key-sort-order 'which-key-key-order-alpha)
  :config
  (which-key-mode)
  (which-key-setup-side-window-bottom)
  nil)

(use-package evil
  :after undo-tree
  :custom
  (evil-want-keybinding nil)
  (evil-want-C-u-scroll t)
  (evil-want-C-d-scroll nil)
  (evil-want-C-i-jump t)
  (evil-show-paren-range 8)
  (evil-undo-system 'undo-tree)
  (evil-kbd-macro-suppress-motion-error t)
  :config
  (evil-mode 1)
  (add-hook 'evil-local-mode-hook 'turn-on-undo-tree-mode))

(use-package evil-matchit
  :config
  (global-evil-matchit-mode 1))

(use-package evil-surround
  :config
  (global-evil-surround-mode 1))

(use-package evil-commentary
    :config
  (evil-commentary-mode 1))

(use-package evil-collection
  :after evil
  :config
  (evil-collection-init))

(use-package multiple-cursors)

(use-package undo-tree
  :config
  (global-undo-tree-mode 1))

(use-package drag-stuff
  :config
  (drag-stuff-global-mode 1))

(require 'vertico)
(map! vertico-map
      "C-k" vertico-previous
      "C-j" vertico-next
      "C-l" jun--insert-and-complete
      "ESC" vertico-exit-input
      "TAB" vertico-insert
      "C-u" vertico-scroll-up
      "C-d" vertico-scroll-down
      "C-r" consult-history)

(require 'evil)

(evil-define-key 'visual global-map (kbd ">") 'my/evil-shift-right)
(evil-define-key 'visual global-map (kbd "<") 'my/evil-shift-left)

(define-key evil-visual-state-map (kbd "J") 'drag-stuff-down)
(define-key evil-visual-state-map (kbd "K") 'drag-stuff-up)
(define-key evil-visual-state-map (kbd "L") 'drag-stuff-right)
(define-key evil-visual-state-map (kbd "H") 'drag-stuff-left)

(define-key evil-normal-state-map (kbd "C-e") 'end-of-line)
(define-key evil-normal-state-map (kbd "C-d") 'evil-scroll-down)
(define-key evil-insert-state-map (kbd "C-g") 'evil--quit-and-goto-normal-mode)
(define-key evil-insert-state-map (kbd "C-d") 'delete-char)
(define-key evil-normal-state-map (kbd "C-f") 'right-char)
(define-key evil-normal-state-map (kbd "C-b") 'left-char)
(define-key evil-insert-state-map (kbd "C-e") 'end-of-line)
(define-key evil-insert-state-map (kbd "C-k") 'end-previous-line)
(define-key evil-insert-state-map (kbd "C-j") 'end-next-line)
(define-key evil-insert-state-map (kbd "C-h") 'evil-delete-backward-char-and-join)
(define-key evil-normal-state-map (kbd "M-p") 'mark-defun)
(define-key evil-normal-state-map (kbd "0") 'evil-next-line-1-first-non-blank)
(define-key evil-normal-state-map (kbd "-") 'end-of-line)
(define-key evil-normal-state-map (kbd "fd") 'evil-write-all)
(define-key evil-normal-state-map (kbd "fr") 'indent-region)
(define-key key-translation-map (kbd "ch") (kbd "C-h"))
(define-key key-translation-map (kbd "cx") (kbd "C-x"))

(evil-global-set-key 'normal "s" nil)
(evil-global-set-key 'normal "so" 'kill-other-buffers)
(evil-global-set-key 'normal "si" 'evil-buffer)
(evil-global-set-key 'normal "sc" 'delete-other-windows)
(evil-global-set-key 'normal "sd" 'kill-this-buffer)
(evil-global-set-key 'normal "sn" 'cycle-buffer-backward)
(evil-global-set-key 'normal "sp" 'cycle-buffer)
(evil-global-set-key 'normal "sk" 'evil-window-top)
(evil-global-set-key 'normal "sj" 'evil-window-bottom)
(evil-global-set-key 'normal "sh" 'evil-window-left)
(evil-global-set-key 'normal "sl" 'evil-window-right)

(evil-global-set-key 'motion "j" 'evil-next-visual-line)
(evil-global-set-key 'motion "k" 'evil-previous-visual-line)


;; improve help on point
(let
    ((+help-command  (if (eq major-mode 'emacs-lisp-mode)
			 '(helpful-at-point)
			'(eldoc-box-help-at-point))))
  (add-hook 'prog-mode-hook
	    (lambda ()
              (define-key evil-normal-state-map (kbd "K") '+help-command))))

(define-key evil-normal-state-map (kbd "C-M-j") 'mc/mmlte--down)
(define-key evil-normal-state-map (kbd "C-M-k") 'mc/mmlte--up)

(use-package cycle-buffer) ;; Allow cycling buffer without falling into `scratch' and `*Messages*' buffers
(evil-global-set-key 'normal "M-n" 'cycle-buffer-backward)
(evil-global-set-key 'normal "M-p" 'cycle-buffer)
(evil-global-set-key 'normal "M-l" 'my-switch-to-previous-buffer)
(evil-global-set-key 'normal "M-k" 'kill-this-buffer)
;;-----------------------------------------------
;; Keymaps definition: 
;;

;; `my-leader-keys' definition :: C-c
(my-leader-keys
    "/" 'consult-line
    "SPC" 'consult-buffer
    "." 'project-find-file
    "," 'projectile-recentf
  
  "a" '(:ignore t :wk "Agenda")
  "a a" '(consult-org-agenda :wk "Open")
  "a c" '(calendar :wk "Calendar")

  "C-j" '(org-journal-new-entry :wk "New entry :[Org Journal]")
  "C-o" '(kill-other-buffers :wk "Kill all buffers but current")
  
  "f" '(:ignore t :wk "File")
  "f r" 'consult-recent-file

  "b" '(:ignore t :wk "Buffer")
  "b k" 'kill-this-buffer
  "b n" 'next-buffer
  "b p" 'previous-buffer
  "b r" 'revert-buffer
  "b i" 'ibuffer
  "b b" 'switch-to-buffer

  "e" '(:ignore t :wk "Evaluate")    
  "e b" 'eval-buffer
  "e d" 'eval-defun
  "e e" 'eval-expression
  "e l" 'eval-last-sexp
  "e r" 'eval-region

  "g" '(:ignore t :wk "Magit")
  "g g" 'magit
  "g p" 'magit-push
  "g P" 'magit-pull
  "g l" 'magit-clone
  "g c" 'magit-commit
  "g s" 'magit-status
  "g t" 'magit-tag
  "g S" 'magit-stash
  "g C" 'magit-cherry
  "g i" 'magit-gitignore
  "g f" 'magit-fetch
  "g w" 'magit-worktree

  "n" '(:ignore t :wk "Org")
  "n c" '(org-roam-capture :wk "[C]apture") 
  "n d" '(deft :wk "Deft Find")
  "n f" '(org-roam-node-find :wk "[F]ind Node")
  "n t" '(org-journal-open-current-journal-file :wk "Journal [T]oday's entry")
  "n o" '(org-journal-new-entry :wk "Open Journal entry")

  "h" '(:ignore t :wk "Help")
  "h c" 'helpful-callable
  "h f" 'helpful-function
  "h C" 'helpful-command
  "h k" '(helpful-key :wk "descript Key") 
  "h m" '(helpful-macro :wk "[M]acros")
  "h p" '(helpful-at-point :wk "help at [P]oint")
  "h s" '(helpful-symbol :wk "[S]ymbol")
  "h r" '(helpful-visit-reference :wk "[R]eference")
  "h u" '(helpful-update :wk "[U]pdate")
  "h v" '(helpful-variable :wk "[V]ariable")
  
  "r" '(:ignore t :wk "Registers/Bookmars")
  "r b" 'consult-bookmark
  "r j" 'bookmark-jump
  "r s" 'bookmark-set
  "r m" 'consult-kmacro
  "r R" 'consult-register-load
  "r S" 'consult-register-store
  "r L" 'consult-register-store

  "c" '(:ignore t)
  "c f" '(consult-flymake t)

  "t" '(:ignore t :wk "Toggle")
  "t t" 'consult-theme
  "t o" 'toggle-org-emphasis-markers

  "s" '(:ignore t :wk "Search")
  "s o" '(consult-outline t)
  "s r" '(consult-ripgrep t)
  "s g" '(consult-grep t)
  "s l" '(consult-goto-line t)

  "o" '(:ignore t)
  "o n" '(lambda () (interactive) (cd "~/docs/") (call-interactively 'find-file))
  "o p" '(lambda () (interactive) (cd user-emacs-directory)(call-interactively 'find-file))

  "p" '(:ignore t :wk "Projects")
  "p a" 'projectile-add-known-project
  );;

(defun +corfu--return ()
  (interactive)
    (if (eq corfu--index 0)
	(progn
	(corfu-first)
	(corfu-insert))
      (corfu-insert)))

(require 'corfu)
(require 'corfu-quick)
(map! corfu-map
      "ESC" nil
      "M-SPC" corfu-insert-separator 
      "#" corfu-insert-separator
      "C-k" corfu-previous
      "C-j" corfu-next
      "C-l" corfu-complete
      "C-d" corfu-scroll-down
      "C-u" corfu-scroll-up
      "SPC" +!jun/quit_and_jump
      "TAB" corfu-insert)

(dolist (c (list (cons "." ".")
                 (cons "," ",")
                 (cons ":" ":")
                 (cons ")" ")")
                 (cons "}" "}")
                 (cons "]" "]")))
  (define-key corfu-map (kbd (car c)) `(lambda ()
                                         (interactive)
                                         (corfu-insert)
                                         (insert ,(cdr c)))))


(my-leader-keys org-mode-map
  "n t" '(org-roam-tag-add :wk "Tag")
  "n g" '(org-roam-graph :wk "[G]raph")
  "n h" '(consult-org-heading :wk "[H]eadings")
  "n ." '(org-roam-node-insert :wk "Insert node on point")
  "n l" '(org-roam-buffer-toggle :wk "Toggle buffer")
  "n a" '(org-roam-alias-add :wk "[A]lias")
  "n i" '(org-id-get-create :wk "[I]d")
  "n r" '(org-roam-ref-add :wk "Add [R]ef"))

;; -------------------------------
;; MISC keybindings

(global-unset-key (kbd "C-z"))
(global-unset-key (kbd "C-x C-c")) ;; Avoid closing emacs unintentionally
(global-set-key (kbd "<escape>") 'keyboard-escape-quit) ;; It always closes frames and gets annoying 
;;(global-unset-key (kbd "<escape>")) ;; Trying to break a bad habit of always pressing ESC when C-g exists

;; Buffers


(global-set-key (kbd "M-%") 'replace-regexp)

(global-set-key (kbd "M-o") 'newline-upward-without-break-of-line)
(global-set-key (kbd "<C-return>") 'newline-without-break-of-line)

;; -------------------------------------------
;; MISC keybinds for =packages=
;; `consult.el' specific 
(global-set-key (kbd "C-x C-r") 'consult-recent-file)
(global-set-key (kbd "C-M-j") 'consult-buffer)
(global-set-key (kbd "C-s") 'consult-line)
(global-set-key (kbd "M-y") 'consult-yank-from-kill-ring)

;; `embark.el'
(global-set-key (kbd "C-.") 'embark-act)

;; `helpful.el'
(global-set-key (kbd "C-h .") 'helpful-at-point)

;; `org'
;; enable todo things
(define-key org-mode-map (kbd "C-c C-d") 'org-toggle-todo-and-fold)
(define-key org-mode-map (kbd "C-M-i") 'completion-at-point)

;;; keymaps.el ends here
